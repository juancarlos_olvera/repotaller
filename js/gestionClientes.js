var clientesObtenidos;

function getClientes() {
  //var theUrl = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers?$filter=contains(Country,%20%27Germany%27)";
  var theUrl = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
            //console.log(xmlHttp.responseText);
            clientesObtenidos = xmlHttp.responseText;
            procesarClientes();
    }
    xmlHttp.open("GET", theUrl, true); // true for asynchronous
    xmlHttp.send(null);
}

function procesarClientes(){
  var JSONClientes = JSON.parse(clientesObtenidos);
  var pathFlag = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";

  var divTabla = document.getElementById("divTabla");

  var tabla = document.createElement("table");
  tabla.classList.add("table-striped");
  var thead = document.createElement("thead");
  //Columns
  var th = document.createElement("th");
  th.innerHTML = "Contact Name";
  thead.appendChild(th);
  th = document.createElement("th");
  th.innerHTML = "City";
  thead.appendChild(th);
  th = document.createElement("th");
  th.innerHTML = "Country";
  thead.appendChild(th);

  tabla.appendChild(thead);
  var tbody = document.createElement("tbody");

  JSONClientes.value.forEach(function(item) {
    //console.log(item.ProductName);
    var country = item.Country;
    var tr = document.createElement("tr");
    var td1 = document.createElement("td");
    var td2 = document.createElement("td");
    var td3 = document.createElement("td");
    var img = document.createElement("img");
    img.classList.add("flag");
    if(country == "UK") {
      country = "United-Kingdom.png";
    } 
    var newPath = pathFlag + country + ".png";
    img.src = newPath;
    td1.innerText = item.ContactName;
    td2.innerText = item.City;
    td3.appendChild(img);

    tr.appendChild(td1);
    tr.appendChild(td2);
    tr.appendChild(td3);
    tbody.appendChild(tr);
  });

  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
