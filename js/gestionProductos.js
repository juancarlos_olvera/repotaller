var productosObtenidos;

function getProductos() {
var theUrl = "https://services.odata.org/V4/Northwind/Northwind.svc/Products?$filter=UnitPrice%20gt%2020";
  var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
            //console.log(xmlHttp.responseText);
            productosObtenidos = xmlHttp.responseText;
            procesarProductos();
        }

    }
    xmlHttp.open("GET", theUrl, true); // true for asynchronous
    xmlHttp.send(null);
}

function procesarProductos(){
  var JSONProductos = JSON.parse(productosObtenidos);
  //console.log(JSONProductos);

  var divTabla = document.getElementById("divTabla");

  var tabla = document.createElement("table");
  tabla.classList.add("table-striped");
  var thead = document.createElement("thead");
  //Columns
  var th = document.createElement("th");
  th.innerHTML = "Product Name";
  thead.appendChild(th);
  th = document.createElement("th");
  th.innerHTML = "Unit Price";
  thead.appendChild(th);
  th = document.createElement("th");
  th.innerHTML = "Units In Stock";
  thead.appendChild(th);

  tabla.appendChild(thead);
  var tbody = document.createElement("tbody");

  JSONProductos.value.forEach(function(item) {
    //console.log(item.ProductName);
    var tr = document.createElement("tr");
    var tdPN = document.createElement("td");
    var tdUP = document.createElement("td");
    var tdUS = document.createElement("td");
    tdPN.innerText = item.ProductName;
    tdUP.innerText = item.UnitPrice;
    tdUS.innerText = item.UnitsInStock;
    tr.appendChild(tdPN);
    tr.appendChild(tdUP);
    tr.appendChild(tdUS);
    tbody.appendChild(tr);
  });

  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
